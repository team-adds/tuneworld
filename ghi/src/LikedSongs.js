import React, { useState, useEffect } from "react";
import "./Table.css";
import "./LikedSongs.css";
import { useAuthContext } from "./useToken";

function LikedSongs() {
  const [songs, setSongs] = useState([]);
  const { token } = useAuthContext();

  useEffect(() => {
    async function fetchData() {
      try {
        await new Promise((r) => setTimeout(r, 1000));
        const response = await fetch(
          `${process.env.REACT_APP_TUNEWORLD_API_HOST}/songs`,
          {
            method: "GET",
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        );
        const data = await response.json();
        setSongs(data);
      } catch (error) {
        console.log(error);
      }
    }
    if (token) {
      fetchData();
    }
  }, [token]);


  function mstoMs(event) {
    const minutes = Math.floor(event / 60000);
    const seconds = ((event % 60000) / 1000).toFixed(0);
    return minutes + ":" + (seconds < 10 ? "0" : "") + seconds;
  }

  return (
    <>
      <div id="child-2" className="songsTable">
        <hr></hr>
        <br></br>
        <div className="tableSubGrid">
          <div className="tableSubChild">
            <table id="songs">
              <thead id="superposition">
                <tr>
                  <th>Title</th>
                  <th>Artist</th>
                  <th>Album</th>
                  <th>Duration</th>
                  <th>Listen</th>
                </tr>
              </thead>
              <tbody>
                {songs?.map((songs, i) => {
                  return (
                    <tr key={i}>
                      <td>{songs.title}</td>
                      <td>{songs.artist}</td>
                      <td>{songs.album}</td>
                      <td>{mstoMs(songs.time)}</td>
                      <td>
                        <a href={songs.preview_url}>Listen Here!</a>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
}
export default LikedSongs;
