import { useState, useEffect } from "react";
import "./ArtistProfile.css";
import { useAuthContext } from "../useToken";

function ArtistBio() {
  const [Account, setAccount] = useState([]);
  const { token } = useAuthContext();

  useEffect(() => {
    async function getAccount() {
    try {
      const url = `${process.env.REACT_APP_TUNEWORLD_API_HOST}/api/accounts/id`;
      const response = await fetch(url, {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
        credentials: "include",
      });
      const data = await response.json();
      setAccount(data);
    } catch (error) {
      console.log(error);
    }
  }
  if (token) {
    getAccount();
  }
  }, [token]);

  return (
    <>
      <br></br>
      <br></br>
      <div className="shortHeading">
        <h3>
          <b>About Me</b>
        </h3>
      </div>
      <br></br>
      <br></br>
      <div className="bioCard">
        <p className="blogText" style={{ color: "white" }}>
          {Account.bio}
        </p>
      </div>
    </>
  );
}

export default ArtistBio;
