



{/* <nav
      style={{ background: "#4B2343" }}
      className=" pt-0 pb-0 navbar navbar-expand-lg navbar-light ">
      <div className="container-fluid">
        <img onClick={toHome}
          src={tuneworld_logo}
          width="45"
          height="30"
          className="d-inline-block align-top"
          alt=""
        ></img>
        <NavLink className="navbar-brand" to="/" style={{ color: "#E23E57" }}>
          Home
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
        {/* if logged in */}
        {IsLoggedIn()}
          {/* if logged out */}

        </div>
      </div>
    </nav> */}



            {/* <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink
                className="nav-link"
                to="/profile"
                style={{ color: "#E23E57" }}>
                Profile
              </NavLink>
            </li>
            </ul>
            <div onClick={logout} className="login">
              LogOut
          </div> */}

construct.js
function Construct(props) {

    const pad2 = num => String(num).padStart(2, '0');

    return (
        <div className="App">
            <header className="App-header">
                <h1>Under construction</h1>
                <h2>Coming on (or before)</h2>
                <h2>{props.info.year}-{pad2(props.info.month)}-{pad2(props.info.day)}</h2>
                <h2>by or <strong>WELL BEFORE</strong> {pad2(props.info.hour)}:{pad2(props.info.min)}</h2>

            </header>
        </div>
    )
}

export default Construct;

import React from "react";
import {
  CDBSidebar,
  CDBSidebarContent,
  CDBSidebarFooter,
  CDBSidebarHeader,
  CDBSidebarMenu,
  CDBSidebarMenuItem,
} from "cdbreact";
import { NavLink } from "react-router-dom";

export default function SideBar() {
  return (
    <div
      style={{ display: "flex", height: "100vh", overflow: "scroll initial" }}
    >
      <CDBSidebar textColor="#fff" backgroundColor="#333">
        <CDBSidebarHeader prefix={<i className="fa fa-bars fa-large"></i>}>
          <a
            href="/"
            className="text-decoration-none"
            style={{ color: "inherit" }}
          >
            TuneWorld
          </a>
        </CDBSidebarHeader>

        <CDBSidebarContent className="sidebar-content">
          <CDBSidebarMenu>
            <NavLink exact to="/">
              <CDBSidebarMenuItem icon="columns">Home</CDBSidebarMenuItem>
            </NavLink>
            <NavLink exact to="/profile">
              <CDBSidebarMenuItem icon="user">Profile</CDBSidebarMenuItem>
            </NavLink>
          </CDBSidebarMenu>
        </CDBSidebarContent>

        <CDBSidebarFooter style={{ textAlign: "center" }}>
          <div
            style={{
              padding: "20px 5px",
            }}
          >
            Created by: Team Adds
          </div>
        </CDBSidebarFooter>
      </CDBSidebar>
    </div>
  );
}


   function Template(){
    return(
        <>
<div class = "all-display">
    <div class = "parent">

{/* link column sticky left */}

<div class = "child-1">
    <div>
        <h3 className="profile">Profile Name</h3>
        <hr></hr>
    </div>
        <ul className="listItem">
            <li>
                <a><b>My Profile</b></a>
            </li>
            <li>
                <a><b>My Favorites</b></a>
            </li>
            <li>
                <a><b>More Links</b></a>
            </li>
        </ul>
</div>
<div id="search">
    <input type="text" id="searchBar" name="fname" placeholder="search"/>
    <button className="button">Search</button>
</div>

<div id="mainpage-child2" class = "mainpage-child2">
<div className="artistName"><h1><b>Artist Name</b></h1></div>

    <div class = "subGrid">
   <div class = "subChild">
<div>



</div>
</div>
    </div>
</div>
</div>
</div>
    </>

    )
   }

   export default Template

    import "./Table.css"
import "./Scratch.css"


function Test (){
    return (
<>
<div id="albumTitle" className="albumTitle">
<img src="https://picsum.photos/200/200" />
<h1 >Album Title</h1>
<hr></hr>

</div>

<div id="child-2" class = "table">


    <div class = "tableSubGrid">
   <div class = "tableSubChild">




<table id="songs">
  <tr className="spaceDown">
    <th>Song name</th>
    <th>Listen here(Spotify link)</th>
    <th>Some Data</th>
  </tr>
  <tr>
    <td>Alfreds Futterkiste</td>
    <td>Maria Anders</td>
    <td>Germany</td>
  </tr>
  <tr>
    <td>Berglunds snabbköp</td>
    <td>Christina Berglund</td>
    <td>Sweden</td>
  </tr>
  <tr>
    <td>Centro comercial Moctezuma</td>
    <td>Francisco Chang</td>
    <td>Mexico</td>
  </tr>
  <tr>
    <td>Ernst Handel</td>
    <td>Roland Mendel</td>
    <td>Austria</td>
  </tr>
  <tr>
    <td>Island Trading</td>
    <td>Helen Bennett</td>
    <td>UK</td>
  </tr>
  <tr>
    <td>Königlich Essen</td>
    <td>Philip Cramer</td>
    <td>Germany</td>
  </tr>
  <tr>
    <td>Laughing Bacchus Winecellars</td>
    <td>Yoshi Tannamuri</td>
    <td>Canada</td>
  </tr>
  <tr>
    <td>Magazzini Alimentari Riuniti</td>
    <td>Giovanni Rovelli</td>
    <td>Italy</td>
  </tr>
  <tr>
    <td>North/South</td>
    <td>Simon Crowther</td>
    <td>UK</td>
  </tr>
  <tr>
    <td>Paris spécialités</td>
    <td>Marie Bertrand</td>
    <td>France</td>
  </tr>
</table>
</div>
</div>
</div>
</>
    )
}
export default Test

    <div>
      <Container>
        <InputGroup className="mb-3" size="lg">
          <Form.Select onChange={(event) => clearSearch(event)} aria-label="Default select example">
            <option>Artist</option>
            <option>Album</option>
            <option>Track</option>
          </Form.Select>
          <FormControl
            placeholder='Search For'
            type="input"
            onKeyPress={(event) => {
              if (event.key === "Enter") {
                console.log(searchValue)
                if (searchValue === "artist") {
                  searchArtists();
                } else if (searchValue === "album") {
                  searchAlbums();
                } else {
                  searchTracks();
                };
              }
            }}
            onChange={(event) => setSearchInput(event.target.value)}
          />
          <Button onClick={(event) => {
            if (searchValue === "artist") {
              searchArtists();
            } else if (searchValue === "album") {
              searchAlbums();
            } else {
              searchTracks();
            }
          }}>Search</Button>
        </InputGroup>
      </Container>


      <div id="mainpage-child2" class = "mainpage-child2">
        <div class = "subGrid">

          {albums.map( (data, i) => {
            if (searchValue == "album") {
              return (
              <div key={i} className = "subChild">
                      <div className="card" >
                      <div className="container" align="center">
                           <div align="center">
                           <img src={data.images[0] ? data.images[0].url : music_pic} />
                           </div>
                           <h4 align="center"><b>{data.name}</b></h4>
                           <p align="center">{data.artists ? data.artists[0].name : null}</p>
                            <div style={{ width: "2rem" }}>
                              <Heart key={i} onClick={() => handleLikeAlbum({uri: data.uri, name: data.name, picture_url: data.images[0].url, artist: data.artists[0].name })} isActive={stateAlbums.includes(data.uri)} animationScale = {1.25} style = {{marginBottom:'1rem'}} />
                            </div>
                       </div>
                       </div>
              </div>
              )

            } else if (searchValue == "artist") {
              return (
              <div key={i} className = "subChild">
                  <div className="card" >
                  <div className="container" align="center">
                        <div align="center">
                        <img src={data.images[0] ? data.images[0].url : music_pic} />
                        </div>
                        <h4 align="center"><b>{data.name}</b></h4>
                        <p align="center">Followers: {data.followers ? data.followers.total : null}</p>
                    </div>
                    </div>
              </div>
              )
            } else if (searchValue == "track") {
              return (
              <div key={i} className = "subChild">
                      <div className="card" >
                      <div className="container" align="center">
                           <div align="center">
                           <img src={data.album ? data.album.images[0].url : music_pic} />
                           </div>
                           <h4 align="center"><b>{data.name}</b></h4>
                           <p align="center">{data.artists ? data.artists[0].name : null}</p>
                       </div>
                       </div>
              </div>
              )
            }
            })}

      </div>
    </div>
    </div>











                  <div className = "subGrid">

            <div className="albumCard" >
            <div className="albumContainer" align="center">
                <div align="center">
                    <img src="https://picsum.photos/300/300" />
                </div>
                <h4 align="center"><b>Album Name</b></h4>
                <p align="center">Album content</p>
            </div>
            </div>
            <div className="albumCard" >
            <div className="albumContainer" align="center">
                <div align="center">
                    <img src="https://picsum.photos/300/300" />
                </div>
                <h4 align="center"><b>Album Name</b></h4>
                <p align="center">Album content</p>
            </div>
            </div>
            <div className="albumCard" >
            <div className="albumContainer" align="center">
                <div align="center">
                    <img src="https://picsum.photos/300/300" />
                </div>
                <h4 align="center"><b>Album Name</b></h4>
                <p align="center">Album content</p>
            </div>
            </div>
            <div className="albumCard" >
            <div className="albumContainer" align="center">
                <div align="center">
                    <img src="https://picsum.photos/200/200" />
                </div>
                <h4 align="center"><b>Album Name</b></h4>
                <p align="center">Album content</p>
            </div>
            </div>
            <div className="albumCard" >
            <div className="albumContainer" align="center">
                <div align="center">
                    <img src="https://picsum.photos/200/200" />
                </div>
                <h4 align="center"><b>Album Name</b></h4>
                <p align="center">Album content</p>
            </div>
            </div>
            <div className="albumCard" >
            <div className="albumContainer" align="center">
                <div align="center">
                    <img src="https://picsum.photos/200/200" />
                </div>
                <h4 align="center"><b>Album Name</b></h4>
                <p align="center">Album content</p>
            </div>
            </div>
            <div className="albumCard" >
            <div className="albumContainer" align="center">
                <div align="center">
                    <img src="https://picsum.photos/200/200" />
                </div>
                <h4 align="center"><b>Album Name</b></h4>
                <p align="center">Album content</p>
            </div>
            </div>
            <div className="albumCard" >
            <div className="albumContainer" align="center">
                <div align="center">
                    <img src="https://picsum.photos/200/200" />
                </div>
                <h4 align="center"><b>Album Name</b></h4>
                <p align="center">Album content</p>
            </div>
            </div>
            <div className="albumCard" >
            <div className="albumContainer" align="center">
                <div align="center">
                    <img src="https://picsum.photos/200/200" />
                </div>
                <h4 align="center"><b>Album Name</b></h4>
                <p align="center">Album content</p>
            </div>
            </div>





                    <div class="content">
                        <ul className="profileItems">
                            <li>
                                <a><b>Favorite Artists</b></a>
                            </li>
                            <li>
                                <a><b>Favorite Albums</b></a>
                            </li>
                            <li>
                                <a><b>Favorite Songs</b></a>
                            </li>
                        </ul>
                    </div>

{/* link column sticky left */}
    <div className="leftColumn">
        <ul>
            <li>
                <a>some link</a>
            </li>
            <li>
                <a>some link</a>
            </li>
            <li>
                <a>some link</a>
            </li>
            <li>
                <a>some link</a>
            </li>
        </ul>
    </div>

    <div className="centerColumn">

        <ul>
            <li>
                <a>some link</a>
            </li>
            <li>
                <a>some link</a>
            </li>
            <li>
                <a>some link</a>
            </li>
            <li>
                <a>some link</a>
            </li>
        </ul>
<div className="centering" align="center">
        <div className="card" >
        <div className="container">
            <img src="https://picsum.photos/200/100"/>
            <h4><b>Alfred husia</b></h4>
            <p>Architect & Engineer</p>
        </div>
        </div>
        <div className="card" >
        <div className="container">
            <img src="https://picsum.photos/200/100"/>
            <h4><b>Alfred husia</b></h4>
            <p>Architect & Engineer</p>
        </div>
        </div>
    </div>
    </div>

    {/* scrollable card column center */}

    {/* external links card column stick right */}





<>
<div>
        <Container fluid claasName="d-inline">
      <Row style={{ height: 200 }} className="overflow-auto">
        <Col className="bg-primary ">
            <div
              className="card m-4 shadow-lg"
              style={{ width: 200 }}
            >
              <img
                className="card-img-top"
                src="https://picsum.photos/200/300"
                alt="Card image cap"
              />
              <div className="card-body">
                <h5 className="card-title">title</h5>
                <h6 className="card-text">artist</h6>
                {/* <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                    <a href="#" className="btn btn-primary">Go somewhere</a> */}
              </div>
            </div>
                        <div
              className="card m-4 shadow-lg"
              style={{ width: 200 }}
            >
              <img
                className="card-img-top"
                src="https://picsum.photos/200/300"
                alt="Card image cap"
              />
              <div className="card-body">
                <h5 className="card-title">title</h5>
                <h6 className="card-text">artist</h6>
                {/* <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                    <a href="#" className="btn btn-primary">Go somewhere</a> */}
              </div>
            </div>
        </Col>
      </Row>
    </Container>
    <Container claasName="inline p-2">
        <Row>
            <Col className="bg-secondary " xs={6}> 2 of 3 (wider)</Col>
        <Col style={{justifyContent:'right'}} className="bg-warning">3 of 3</Col>
        </Row>
        </Container>
        </div>
        </>

<Container fluid >
  <Row fluid>
    <Col className=" bg-primary  span2">
      <a>link</a>

        <Row>
      <a>link</a>
      </Row>
    <Row>
      <a>link</a>
      </Row>
    </Col>
    <Col class="overflow-auto span10">
       <div >
            <div
              className="card m-4 shadow-lg"
              style={{ width: 200 }}
            >
              <img
                className="card-img-top"
                src="https://picsum.photos/200/300"
                alt="Card image cap"
              />
              <div className="card-body">
                <h5 className="card-title">title</h5>
                <h6 className="card-text">artist</h6>
                {/* <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                    <a href="#" className="btn btn-primary">Go somewhere</a> */}
              </div>
            </div>
            <div
              className="card m-4 shadow-lg"
              style={{ width: 200 }}
            >
              <img
                className="card-img-top"
                src="https://picsum.photos/200/300"
                alt="Card image cap"
              />
              <div className="card-body">
                <h5 className="card-title">title</h5>
                <h6 className="card-text">artist</h6>
                {/* <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                    <a href="#" className="btn btn-primary">Go somewhere</a> */}
              </div>
            </div>
            <div
              className="card m-4 shadow-lg"
              style={{ width: 200 }}
            >
              <img
                className="card-img-top"
                src="https://picsum.photos/200/300"
                alt="Card image cap"
              />
              <div className="card-body">
                <h5 className="card-title">title</h5>
                <h6 className="card-text">artist</h6>
                {/* <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                    <a href="#" className="btn btn-primary">Go somewhere</a> */}
              </div>
            </div>
        </div>
    </Col>
  </Row>
</Container>
    </>



Scratch.css

* page background */

body{
    background-color: rgb(42,24,59);
}

/* page grid */

.parent {
display: grid;
grid-template-columns: 25vh auto 25vh;
grid-template-rows: 40px auto auto auto;
grid-row-gap: 5px;
grid-column-gap: 10px;
height:95vh;

}
.parent > div {

margin-top: 4px;
padding: 5px;
border-radius: 5px;
background-color: rgb(127, 51, 77);
}

/* profile column */

    .child-1  {
    grid-row-start: 1;
    grid-row-end: 5;
    }
    .profile{
        text-shadow: 1px 1px 2px rgb(165, 55, 265);
    }

        .listItem{
            list-style: none;
            margin: 0px;
            padding:0px;
        }
        .listItem > li{
            margin: 5px 5px 5px 5px;
            padding: 5px 5px 5px 5px;
            color: rgb(42,24,59)

        }
        .listItem > li:hover{
        background-color: rgb(209, 74, 90);
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        }

/* Search Bar */

    #search{
        background-color: rgb(42,24,59);

    }
    #searchBar{
        padding-left: 20px;
        border-radius: 25px 0px 0px 25px;
        box-shadow: 0 4px 8px 0 rgba(127, 51, 77,0.9);
    }
    .button{
        background:linear-gradient( rgb(65, 112, 246),rgb(165, 55, 265));
        border-radius: 0px 25px 25px 0px;
        box-shadow: 0 4px 8px 0 rgba(127, 51, 77,0.9);

    }
    .button:hover{
        box-shadow: inset 0 0 6px rgba(0, 0, 0, .5)
    }

/* scrollable center component */
    .child-2{
    grid-row-start: 2;
    grid-row-end: 5;
    overflow-y:scroll;
    }
    #child-2{
    background-color: rgb(42, 24, 59);
    color: rgb(209, 74, 90);
    }
    .artistName{
    background-color: rgb(118, 56, 77);
    position:sticky;
    top:0;
    width:30%;
    height:7vh;
    z-index:100;
    padding: 0vh;
    box-shadow: 0 4px 8px 0 rgba(127, 51, 77,0.9);
    }
    .artistName b{
        padding-left: 20px;
        text-shadow: 1px 1px 2px rgb(165, 55, 265);
        color:black
    }

        /* subgrid layout */

        .subGrid {
        display: grid;
        grid-template-columns: 1fr 1fr 1fr;
        }
        .subChild {
        margin-top: 0px;
        width: auto;
        padding: 5px;
        border-radius: 5px;
        background-color: rgb(42, 24, 59);

        }
    #child-3{
        background-color: rgb(42, 24, 59);
        color: rgb(209, 74, 90);
        text-align: center;
    }



/* card components */

 .card {
  /* Add shadows to create the "card" effect */
  /* background-color: rgb(209,74,90); */
  background-color: rgb(42,24,59);
  /* box-shadow: 0 4px 8px 0 rgba(127, 51, 77,0.9); */
  /* transition: 0.3s; */
  border:0px;
 }
 .card h4{
    color:aliceblue;
 }
  .card p{
    color:aliceblue;
 }

img{
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);

}

/* On mouse-over, add a deeper shadow */
.card:hover  {
background-color: rgb(209,74,90);
  box-shadow: 0 8px 16px 0 rgba(255,255,255,0.2);

}
 .card:hover h4{
color: rgb(42,24,59)
 }

  .card:hover p{
color: rgb(42,24,59)
 }
/* Add some padding inside the card container */
.container {
    display:grid;
    grid-template-columns: auto;
    grid-row-gap: 5px;
    grid-column-gap: 40px;
    margin-top: 20px;
    color:black
}
