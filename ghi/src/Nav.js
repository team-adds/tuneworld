import { NavLink, useNavigate } from "react-router-dom";
import tuneworld_logo from "./images/music.png";
import "./nav.css";
import { useState, useEffect } from "react";
import { useAuthContext } from "./useToken";

function Nav() {
  const navigate = useNavigate();

  async function logout() {
    const url = `${process.env.REACT_APP_TUNEWORLD_API_HOST}/token`;
    await fetch(url, { method: "delete", credentials: "include" });
    navigate("/");
    window.location.reload(false);
  }

  const [currAccount, setCurrAccount] = useState(0);
  const [name, setName] = useState("");
  const { token } = useAuthContext();

  useEffect(() => {
    if (token) {
      fetch(`${process.env.REACT_APP_TUNEWORLD_API_HOST}/token`, {
        credentials: "include",
      })
        .then((response) => response.json())
        .then((data) => {
          setCurrAccount(data.account.id);
          setName(data.account.full_name);
        });
    }
  }, [currAccount, token]);

  function IsProfileIn() {
    if (token) {
      return (
        <NavLink
          className="nav-link"
          to="/profile"
          style={{ color: "#E23E57" }}
        >
          Profile
        </NavLink>
      );
    }
  }
  function IsLoggedIn() {
    if (token) {
      return (
        <>
          <div>
            <p className="name" style={{ color: "#E23E57" }}>
              Welcome, {name}
            </p>
          </div>

          <div className="signup" onClick={logout}>
            <NavLink className="nav-link" to="/">
              LogOut
            </NavLink>
          </div>
        </>
      );
    }
  }
  function IsLoggedOut() {
    if (!token) {
      return (
        <>
          <div className="login">
            <NavLink className="nav-link" to="/login">
              Login
            </NavLink>
          </div>
          <div className="signup">
            <NavLink className="nav-link" to="/signup">
              SignUp
            </NavLink>
          </div>
        </>
      );
    }
  }
  return (
    <nav
      style={{ background: "#4B2343" }}
      className=" pt-0 pb-0 navbar navbar-expand-lg fixed-top navbar-light "
    >
      <div className="container-fluid">
        <img
          src={tuneworld_logo}
          height="30"
          className="d-inline-block align-top"
          alt=""
        ></img>
        <NavLink className="navbar-brand" to="/" style={{ color: "#E23E57" }}>
          Home
        </NavLink>
        {IsProfileIn()}
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item"></li>
          </ul>
          {IsLoggedIn()}
          {IsLoggedOut()}
        </div>
      </div>
    </nav>
  );
}

export default Nav;
