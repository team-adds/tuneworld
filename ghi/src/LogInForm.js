import { useState } from "react";
import { useToken } from "./useToken";
import { useNavigate } from "react-router-dom";

function LoginComponent() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [token, login] = useToken();
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const resp = await login(email, password);
      if (resp !== false) {
        setEmail("");
        setPassword("");
        navigate("/");
        window.location.reload(false);
      } else {
        alert("Invalid email or password");
      }
      console.log(token);
    } catch (error) {
      console.log(error);
      console.log("Wrong email or password!");
    }
  };

  return (
    <div className="positioning">
      <div className="container2">
        <form onSubmit={handleSubmit}>
          <div className="mb-3">
            <label
              htmlFor="email"
              className="form-label"
              style={{ color: "#E23E57" }}
            >
              Email address
            </label>
            <input
              onChange={(e) => setEmail(e.target.value)}
              value={email}
              type="email"
              className="form-control"
              id="email"
            />
          </div>
          <div className="mb-3">
            <label
              htmlFor="password"
              className="form-label"
              style={{ color: "#E23E57" }}
            >
              Password
            </label>
            <input
              onChange={(e) => setPassword(e.target.value)}
              value={password}
              type="password"
              className="form-control"
              id="password"
            />
          </div>
          <button
            className="btn btn-outline-light my-2 my-sm-0"
            type="submit"
            style={{ color: "#E23E57" }}
          >
            Submit
          </button>
        </form>
      </div>
    </div>
  );
}
export default LoginComponent;
