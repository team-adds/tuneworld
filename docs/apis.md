## APIs

# Account
* Method: `GET`, `POST`
* Path: `/api/accounts`

Input POST:

```
{
  "email": "string",
  "password": "string",
  "full_name": "string",
  "bio": "string",
  "picture_url": "string
}
```

Output GET:

```
[
  {
    "id": "string",
    "email": "string",
    "full_name": "string",
    "bio": "string",
    "picture_url": "string
  }
]
```

Creates a new account with an email and password and assigns an id. The bio and picture are optional and can also be editted from the profile edit page

# Token
* Method: `GET`, `POST`, `DELETE`
* Path: `/api/token`

Input `POST`:
```
{
    "username": "string",
    "password": "string"
}
```

Output `GET`:
```
{
  "access_token": "string",
  "token_type": "Bearer",
  "account": {
    "id": "string",
    "email": "string",
    "full_name": "string"
  }
}
```

The token api handles logging in/out as well as handles fetching the token for a logged in account.

# Artists
* Method: `GET`, `POST`, `DELETE`, `GET`
* Paths: `/artists/{name}`, `/artists`, `/artists/{uri}/{account_id}`, `/artists/{account_id}/`

Input `POST`
```
{
  "name": "string",
  "uri": "string",
  "picture_url": "string",
  "account_id": 0
}
```
Output `GET`
```
[
  {
    "name": "string",
    "uri": "string",
    "picture_url": "string",
    "account_id": 0,
    "id": 0
  }
]
```

Liking an artist will post the artist and information to our database along with the id of the account that liked it. We can get the artists by either the name of the artists, or we can get a list of all artists liked by the account_id. The delete is what is called when you unlike an artist and it deletes it based on the artists' specific uri and the account id that has it liked

# Albums
* Method: `POST`, `GET`, `DELETE`, `GET`
* Paths: `/songs`, `/songs/{title}`, `/songs/{uri}/{account_id}`, `/songs/{account_id}/`

Input `POSt`:
```
{
  "name": "string",
  "uri": "string",
  "picture_url": "string",
  "artist": "string",
  "account_id": 0
}
```
Output `GET`:
```
[
  {
    "name": "string",
    "uri": "string",
    "picture_url": "string",
    "artist": "string",
    "account_id": 0,
    "id": 0
  }
]
```

The Albums work the same as the Artists apis. Post happens when an account likes an artist. Delete happens when you delete one. We are able to get an album by either its name, or a list of all liked albums by a specific account.

# Songs
* Method: `POST`, `GET`, `DELETE`, `GET`
* Paths: `/songs`, `/songs/{title}`, `/songs/{uri}/{account_id}`, `/songs/{account_id}/`

Input `POST`:
```
{
  "uri": "string",
  "title": "string",
  "artist": "string",
  "time": 0,
  "album": "string",
  "preview_url": "string",
  "account_id": 0
}
```
Output `GET`:
```
[
  {
    "uri": "string",
    "title": "string",
    "artist": "string",
    "time": 0,
    "album": "string",
    "preview_url": "string",
    "account_id": 0,
    "id": 0
  }
]
```

The songs works the same as the artists and albums apis. Liking a song on the front-end posts it to the database linked with the account that liked it. We can get a song by either its name, or get a list of them based on the account_id that liked it.

# Spotify
* Methods: `GET`
* Path: `/api/spotify/{artist}`, `/api/spotify/album/{uri}`

Input:
```
{
    "string"
}
```

Output:
```
{
  "detail": [
    {
      "loc": [
        "string",
        0
      ],
      "msg": "string",
      "type": "string"
    }
  ]
}
```
