steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE accounts (
            id SERIAL PRIMARY KEY NOT NULL,
            email VARCHAR(100) NOT NULL UNIQUE,
            password VARCHAR(100) NOT NULL,
            full_name VARCHAR(100) NOT NULL,
            bio VARCHAR(1000),
            picture_url VARCHAR(2048)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE accounts;
        """,
    ],
    [
        """
        CREATE TABLE artists (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(100) NOT NULL,
            uri VARCHAR(100) NOT NULL,
            picture_url VARCHAR(2048),
            account_id INT NOT NULL REFERENCES accounts(id)
        );
        """,
        """
        DROP TABLE artists;
        """,
    ],
    [
        """
        CREATE TABLE albums (
            id SERIAL PRIMARY KEY NOT NULL,
            uri VARCHAR(100) NOT NULL,
            name VARCHAR(100) NOT NULL,
            picture_url VARCHAR(250),
            artist VARCHAR(100),
            account_id INT NOT NULL REFERENCES accounts(id)
        );
        """,
        """
        DROP TABLE albums;
        """,
    ],
    [
        """
        CREATE TABLE songs (
            id SERIAL PRIMARY KEY NOT NULL,
            uri VARCHAR(100) NOT NULL,
            title VARCHAR(100) NOT NULL,
            time INT NOT NULL,
            artist VARCHAR(100) NOT NULL,
            album VARCHAR(100) NOT NULL,
            preview_url VARCHAR(150),
            account_id INT NOT NULL REFERENCES accounts(id)

        );
        """,
        """
        DROP TABLE songs;
        """,
    ],
]
