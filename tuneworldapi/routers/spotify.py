from fastapi import APIRouter, Depends
from queries.spotify import SpotifyQueries


router = APIRouter()


@router.get("/api/spotify/{artist}")
def get_albums(artist: str, repo: SpotifyQueries = Depends()):
    return repo.get_album_by_artist(artist)


@router.get("/api/spotify/album/{uri}")
def get_tracks(uri: str, repo: SpotifyQueries = Depends()):
    return repo.get_tracks_by_album_uri(uri)
