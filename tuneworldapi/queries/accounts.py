from pydantic import BaseModel
from typing import List, Optional
from queries.pool import pool


class DuplicateAccountError(ValueError):
    pass


class AccountIn(BaseModel):
    email: str
    password: str
    full_name: str
    bio: str | Optional[None]
    picture_url: str | Optional[None]


class AccountOut(BaseModel):
    id: str
    email: str
    full_name: str
    bio: str | Optional[None]
    picture_url: str | Optional[None]


class AccountOutWithPassword(AccountOut):
    hashed_password: str


class AccountUpdateIn(BaseModel):
    bio: str | Optional[None]
    picture_url: str | Optional[None]


class AccountUpdateOut(AccountUpdateIn):
    id: str


class AccountQueries:
    def get(self, email: str) -> AccountOutWithPassword:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                        SELECT *
                        FROM accounts
                        WHERE email = %s
                    """,
                    [email],
                )
                for row in cur.fetchall():
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                    return record

    def get_account_by_id(self, Account_id: str) -> AccountOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                        SELECT id, email, full_name, bio, picture_url
                        FROM accounts
                        WHERE id = %s
                    """,
                    [Account_id],
                )

                record = cur.fetchone()
                account = AccountOut(
                    id=record[0],
                    email=record[1],
                    full_name=record[2],
                    bio=record[3],
                    picture_url=record[4]
                )
                return account

    def create(
        self, info: AccountIn, hashed_password: str
    ) -> AccountOutWithPassword:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                        INSERT INTO accounts
                            (email,
                            password,
                            full_name,
                            bio,
                            picture_url)
                        VALUES
                            (%s, %s, %s,%s,%s)
                        RETURNING id,
                        email,
                        password,
                        full_name,
                        bio,
                        picture_url;
                    """,
                    [
                        info.email,
                        hashed_password,
                        info.full_name,
                        info.bio,
                        info.picture_url,
                    ],
                )
                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                return record

    def get_all(self) -> List[AccountOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, email, full_name
                        FROM accounts
                        ORDER BY full_name;
                        """
                    )
                    lst = []
                    for record in db:
                        account = AccountOut(
                            id=record[0],
                            email=record[1],
                            full_name=record[2]
                        )
                        lst.append(account)
                    return lst
        except Exception:
            return {"message": "Accounts not found"}

    def update_user(
        self, info: AccountUpdateIn, account_id: int
    ) -> AccountUpdateOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                            UPDATE accounts
                            SET bio = %s
                            , picture_url = %s

                            WHERE id = %s
                            RETURNING id, bio, picture_url;
                            """,
                        [
                            info.bio,
                            info.picture_url,
                            account_id
                        ],
                    )
                    record = None
                    row = cur.fetchone()
                    if row is not None:
                        record = {}
                        for i, column in enumerate(cur.description):
                            record[column.name] = row[i]
                    return record
        except Exception as e:
            print(e)
            return {"message": "Could not update user"}
