from pydantic import BaseModel
from queries.pool import pool
from typing import List, Optional


class Error(BaseModel):
    message: str


class SongIn(BaseModel):
    uri: str
    title: str
    artist: str
    time: int
    album: str
    preview_url: str
    account_id: Optional[int]


class SongOut(SongIn):
    id: int


class SongRepository:
    def create(self, song: SongIn):
        try:
            # connect the database.
            # 'with' is a monitor which allows us to skip try except blocks
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    # Run our INSERT statement
                    result = db.execute(
                        """
                        INSERT INTO songs
                            (uri,
                            title,
                            time,
                            artist,
                            album,
                            preview_url,
                            account_id)
                        VALUES
                            (%s, %s, %s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            song.uri,
                            song.title,
                            song.time,
                            song.artist,
                            song.album,
                            song.preview_url,
                            song.account_id,
                        ],
                    )
                    id = result.fetchone()[0]
                    # Return new data
                    old_data = song.dict()
                    return SongOut(id=id, **old_data)

        except Exception:
            return {"message": "Create did not work"}

    def get(self, title: str) -> SongOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT *
                        FROM songs
                        WHERE title = %s
                        """,
                        [title],
                    )
                    for row in db.fetchall():
                        record = {}
                        for i, column in enumerate(db.description):
                            record[column.name] = row[i]
                        return record
        except Exception as e:
            print(e)
            return {"message": "Could not get that song"}

    def delete(self, uri: str, account_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM songs
                        WHERE uri = %s AND account_id = %s
                        """,
                        [uri, account_id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def get_all(self, account_id: int) -> List[SongOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id,
                        uri,
                        title,
                        time,
                        artist,
                        album,
                        preview_url,
                        account_id
                        FROM songs
                        WHERE account_id = %s
                        ORDER BY artist;
                        """,
                        [account_id],
                    )
                    lst = []
                    for record in db:
                        song = SongOut(
                            id=record[0],
                            uri=record[1],
                            title=record[2],
                            time=record[3],
                            artist=record[4],
                            album=record[5],
                            preview_url=record[6],
                            account_id=record[7],
                        )
                        lst.append(song)
                    return lst
        except Exception:
            return {"message": "Could not get all songs"}
