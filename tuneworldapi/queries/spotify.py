import spotipy
from spotipy.oauth2 import SpotifyClientCredentials
import os


class SpotifyQueries:

    # This is for getting all the albums

    def get_album_by_artist(self, artist: str):
        client_credentials_manager = SpotifyClientCredentials(
            os.environ["API_CLIENT_ID"],
            os.environ["API_CLIENT_SECRET"],
        )
        sp = spotipy.Spotify(
            client_credentials_manager=client_credentials_manager
        )

        sp_dict = sp.search(artist, type="artist", limit=5)
        artist_id = sp_dict["artists"]["items"][0]["id"]
        artist_albums_dict = sp.artist_albums(
            # all albums by an artist
            artist_id, album_type="album", limit=10
        )

        album_dict = []
        # iterate through all of spotify's albums
        for i in artist_albums_dict["items"]:
            # regenerate all the names of the albums in our list each time
            all_names = []
            for j in album_dict:
                all_names.append(j["name"])
            # if the spotify album is already in our dictionary list,
            # add the new URI
            if i["name"] in all_names:
                for d in album_dict:
                    if i["name"] == d["name"]:
                        d["uri"].append(i["uri"])
            # else, create a new entry in our list of dictionaries
            else:
                _dict = {}
                _dict["name"] = i["name"]
                _dict["uri"] = [i["uri"]]
                _dict["image_url"] = i["images"][0]["url"]
                _dict["artist"] = i["artists"][0]["name"]
                album_dict.append(_dict)
        return album_dict

    def get_tracks_by_album_uri(self, uri):
        client_credentials_manager = SpotifyClientCredentials(
            os.environ["API_CLIENT_ID"],
            os.environ["API_CLIENT_SECRET"],
        )
        sp = spotipy.Spotify(
            client_credentials_manager=client_credentials_manager
        )
        song_dict = {}
        tracksList = []
        _dict1 = {}
        album_info = sp.album(uri)
        # all the tracks from an album
        track_info = album_info["tracks"]["items"]
        # Storing the info we need for each track into a dictionary
        for track in track_info:
            # album name
            _dict1["album"] = album_info["name"]
            _dict1["album_artwork"] = album_info["images"][2]["url"]
            # artist name
            _dict1["artist"] = track["artists"][0]["name"]
            _dict1["title"] = track["name"]
            _dict1["id"] = track["id"]
            min = str(track["duration_ms"]//60000)
            sec = (track["duration_ms"]//1000) % 60
            if sec < 10:
                sec = f'0{sec}'
            _dict1["duration"] = f'{min}:{sec}'
            _dict1["preview"] = track["preview_url"]
            # Appending each track dictionary into the list of tracks
            tracksList.append(_dict1)
            # Clearing out the dictionary to create the next track dictionary
            _dict1 = {}
        # Adding the list of tracks into a dictionary
        song_dict["track_info"] = tracksList
        return song_dict
