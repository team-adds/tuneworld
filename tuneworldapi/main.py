from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from authenticator import authenticator
from routers import accounts, songs, spotify, artists, albums
import os

app = FastAPI()

origins = ["https://team-adds.gitlab.io",
           "http://localhost:3000", os.environ.get("CORS_HOST", None)]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(authenticator.router, tags=["Authentication"])
app.include_router(accounts.router, tags=["Authentication"])
app.include_router(spotify.router, tags=["Spotify"])
app.include_router(artists.router, tags=["Artists"])
app.include_router(albums.router, tags=["Albums"])
app.include_router(songs.router, tags=["Songs"])
